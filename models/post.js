var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var commentSchema = new Schema();

commentSchema.add({
	date: { type: Date, default: Date.now },
	poster: String,
	comments: [commentSchema],
	isByAdmin: Boolean
});

var postSchema = new Schema({
    date: { type: Date, default: Date.now },
    poster: String,
    text: String,
    tags: [String],
    comments: [commentSchema]
});

Comment = mongoose.model("Comment", commentSchema);
Post = mongoose.model("Post", commentSchema);

module.exports = [Comment,Post];