var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/leafshade');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

links = [
    { url: '/blog', name: 'blog'},
    { url: '/games/', name: 'games'},
    { url: '/gallery/calin', name: 'gallery'}
  ];

app.use(function(req,res,next){
    res.locals.url = req.url;
    res.locals.links = links
    next();
});

///load models

fs.readdirSync('./models').forEach(function (file) {
    if(file.substr(-3) == '.js') {
        require('./models/' + file);
    }
});

/// load controllers

fs.readdirSync('./controllers').forEach(function (file) {
    if(file.substr(-3) == '.js') {
        require('./controllers/' + file)(app);
    }
});

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
