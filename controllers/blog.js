
module.exports = function (app)
{
	
	var postsPerPage = 20;

	app.get("/blog", function (req,res){
		res.redirect("/blog/page/1");
	});

	app.get("/blog/page/:pageNumber", function (req,res,next){
		var page = req.params.pageNumber;
		Post.count(function (err,c)
		{
			var pageCount = Math.ceil(postsPerPage / c);
			if (pageCount < page)
			{
				var e = new Error();
				e.status = 404;
				next(e);
			}
			else
			{
				Post.find().sort("date").skip((page - 1) * postsPerPage).limit(postsPerPage).exec(function(posts) {
					if (!posts)
					{
						var e = new Error();
						e.status = 404;
						next(e);
					}
					else
					{
						res.render("page", { posts: posts, pageCount: pageCount})
					}
				});
			}
		});
		
	});

	app.get("/", function (req,res) {
		res.redirect("/blog");
	});

}